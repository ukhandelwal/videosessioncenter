var session = {
    video: true,
    audio: true
};
var recorder = new MRecordRTC();
var audioRecorder = new MRecordRTC();
function getUserMedia() {
    navigator.getUserMedia || (navigator.getUserMedia = navigator.mozGetUserMedia
                                 || navigator.webkitGetUserMedia || navigator.msGetUserMedia);
    navigator.getUserMedia(session, onSuccess, onError);
}

function onSuccess(stream) {
    mediaStream = stream;
    window.stream = mediaStream;
    var videostream = mediaStream.clone();
    recorder.addStream(videostream);
    recorder.mediaType = {
        video: "videorec"
    };
    audioRecorder.addStream(mediaStream);
    audioRecorder.mediaType = {
        audio: "audiorec"
    }
    recorder.startRecording();
    audioRecorder.startRecording();

    setVideo(mediaStream, "localvideotag");
}
function onError(stream) {
    alert("error in " + stream);
}

function setVideo(stream, tag) {
    var source;
    var videoStreamTag = document.getElementById(tag);
    if (window.webkitURL) {
        source = window.webkitURL.createObjectURL(stream);
    } else {
        source = window.URL.createObjectURL(stream);
    }
    videoStreamTag.src = source;
    videoStreamTag.setAttribute("controls", "controls");
    videoStreamTag.autoplay = true;
    videoStreamTag.defaultMuted = true;
}

function stopRecordingVideo() {
    recorder.stopRecording(function (url, type) {
        console.log("url " + url + " type " + type);
        $('#remotevideotag').src = url;
        recorder.save({
            video: true
        });
    });
    audioRecorder.stopRecording(function (url, type) {
        console.log("url " + url + " type " + type);
        audioRecorder.save({
            audio:true
        });
    })
    window.stream.stop();
}


function recordRtcWriteToDisk(videoBlob) {
    RecordRTC.writeToDisk({
        video: videoBlob
    });
}

function getRecordedVideo() {
    RecordRTC.getFromDisk('all', function (dataURL, type) {
        if (type === 'audio') {
            document.getElementById("blobs").innerHTML += "<br>Audio Blob : " + dataURL;
        }
        if (type === 'video') {
            document.getElementById("blobs").innerHTML += "<br>VIdeo Blob : " + dataURL;
        }
        if (type === 'gif') {
            document.getElementById("blobs").innerHTML += "<br>GIF Blob : " + dataURL;
        }
    });
}