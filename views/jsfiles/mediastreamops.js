var mainstream;
navigator.getUserMedia || (navigator.getUserMedia = navigator.mozGetUserMedia
                                 || navigator.webkitGetUserMedia || navigator.msGetUserMedia);
function getUserMediaStream(width, height, improve) {
    $('.container').css("display", "");
    console.log("into get User media");
    // This check makes sure that getUserMedia is available.
    if (navigator.getUserMedia) {
        // this is to test if the person is opening the site in a mobile browser.
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            isPc = false;
            var qvgaConstraints = {
                video: {
                    mandatory: {
                        maxWidth: width,
                        maxHeight: height
                    }
                },
                audio: true

            };
            // access the media of the phone.
            navigator.getUserMedia(qvgaConstraints, onSuccess, onError);
        } else {
            isPc = true;
            var qvgaConstraints = {
                video: {
                    mandatory: {
                        maxWidth: width,
                        maxHeight: height
                    }
                },
                audio: true

            };
            //access the media of the computer.
            console.log("calling getUserMedia ");
            navigator.getUserMedia(qvgaConstraints, onSuccess, onError);
        }
    } else {
        alert("getUserMedia not supported");
    }
}


function onSuccess(stream) {
    console.log("Is the stream causing issue");
    window.stream = stream;
    var browser = navigator.userAgent.toLowerCase();
    //some of the operations are firefox centric and hence the check.
    if (browser.indexOf('firefox') > -1) {
        isFirefox = true;
    } else {
        isFirefox = false;
    }


    if (typeof stream.clone === 'function') {
        mediastream = stream.clone();
    } else {
        // to be enabled for remote testing purposes only.
        //   alert("Cloning is not supported in this browser!!");
        console.log("Cloning is not supported in this browser!!");
        mediastream = stream;
    }
    mainstream = stream;
    // i have joined. send this information to the server.
    setVideoLocal(mediastream, qualityModify);
    if (!qualityModify) {
        // sending information of the peer to the server. The peer is ready to accept connections now.
        sendRtcInfo(localPeerId, roombooked, usernametaken);
    } else {
        // peer.disconnect();
        //  peer.reconnect();
        qualityModify = false;
    }


}
function onError(stream) {
    alert("error in " + stream);
}

function hideModal() {
    $('#waitingModal').modal('hide');
    $('#waitingModal').on('hidden.bs.modal', '.modal', function () {
        $(this).removeData('bs.modal');
    });
}
function setVideoLocal(stream) {
    var source;
    //var videoStream = document.getElementById(id);
    var videoStream;
    var button;
    var hdButton;

        videoStream = document.createElement("video");
        //  videoStream.id = id;
        var division = document.createElement("div");
        //     division.classList.add("video-container");
        division.appendChild(videoStream);
        button = getMicMuteButton();
        hdButton = getHDButton();
        division.appendChild(button);
        division.appendChild(hdButton);
        if (isFirefox) {
            addEventListenerBtn(button, videoStream);
            enableDisableAudioTrackFirefox(button, videoStream);
            improveQuality(hdButton, videoStream);
        } else {
            addMuteEventListenerBtn(button, videoStream);
            enableDisableAudioTrackChrome(button, videoStream);
            improveQuality(hdButton, videoStream);
        }

        document.getElementById('other-videos').appendChild(division);
        if (window.webkitURL) {
            source = window.webkitURL.createObjectURL(stream);
        } else {
            source = window.URL.createObjectURL(stream);
        }
        videoStream.src = source;
        videoStream.setAttribute("controls", "controls");
        videoStream.autoplay = true;
        addEventListenerVideoStreamTag(videoStream);
    // setClassName();
}
var mainVideoFilled = false;
function setVideo(stream) {
    var source;
    //var videoStream = document.getElementById(id);
    var videoStream;
    var button;
    if (videoStream == null) {
        videoStream = document.createElement("video");
        //  videoStream.id = id;
        var division = document.createElement("div");
        division.classList.add("video-container");
        division.appendChild(videoStream);
        if (!mainVideoFilled) {
            document.getElementById('main-playing-video').appendChild(division);
            mainVideoFilled = true;
        } else {
            document.getElementById('other-videos').appendChild(division);
        }

    }

    if (window.webkitURL) {
        source = window.webkitURL.createObjectURL(stream);
    } else {
        source = window.URL.createObjectURL(stream);
    }
    videoStream.src = source;
    videoStream.setAttribute("controls", "controls");
    videoStream.autoplay = true;
    //  setClassName();
    addEventListenerVideoStreamTag(videoStream);
    return videoStream;
}

function addEventListenerVideoStreamTag(videoStreamTag) {
    videoStreamTag.addEventListener('dblclick', function (e) {
        // convert the video dom element into a jQuery Object.
        var smallVideo = $(videoStreamTag); ;
        var largeVideo = $('#main-playing-video').children().first();
        var smallVideoElem = smallVideo.detach();
        var largeVideoElem = largeVideo.detach();
        ($('#main-playing-video')).append(smallVideoElem);
        ($('#other-videos')).append(largeVideoElem);
        setTimeout(function () {
            videoStreamTag.play();
        }, 500);
        return false;
    });
}

function setClassName() {
    var elements = document.getElementById('videodivision').children;
    var elementsNo = $('.video-container').length;
    console.log('No of elements ' + elementsNo + " length " + elements.length);
    if (elementsNo == 1) {
        addClassNameToAllElements('', 'one', elements);
    }
    if (elementsNo == 2) {
        addClassNameToAllElements('one', 'two', elements);
    }
    if (elementsNo == 3) {
        addClassNameToAllElements('two', 'three', elements);
    }
    if (elementsNo >= 4) {
        addClassNameToAllElements('three', 'four', elements);
    }
}

function addClassNameToAllElements(oldClass, newClass, elements) {
    $(document).ready(function () {
        $('.video-container').removeClass(oldClass);
        $('.video-container').addClass(newClass);
    });
}

function getMicMuteButton() {
    var button = document.createElement('button');
    button.setAttribute("class", "mutebutton btn btn-default btn-primary");
    var iconspan = document.createElement('span');
    iconspan.className = 'fa fa-microphone';
    button.appendChild(iconspan);
    return button;
}

function getHDButton() {
    var hdbutton = document.createElement('button');
    hdbutton.setAttribute("class", "hdbutton btn btn-default btn-primary");
    var iconspan = document.createElement('span');
    iconspan.className = 'fa fa-expand';
    hdbutton.appendChild(iconspan);
    return hdbutton;
}

function enableDisableAudioTrackChrome(button, videoStreamDiv) {
    if (button.firstChild) {
        button.removeChild(button.firstChild);
    }
    if (mediastream.getAudioTracks().length > 0) {
        // time to mute the track as it was unmuted before.
        var audioTracks = mediastream.getAudioTracks();
        for (var i = 0; i < audioTracks.length; i++) {
            console.log("Muting media stream ");
            audioTracks[i].stop();
            mediastream.removeTrack(audioTracks[i]);
        }
        var iconspan = document.createElement('span');
        iconspan.className = 'fa fa-microphone-slash';
        button.appendChild(iconspan);

    } else {
        // time to unmute the track as  it was muted before.
        var audioTracks = mainstream.getAudioTracks();
        for (var i = 0; i < audioTracks.length; i++) {
            console.log("Unmuting media stream ");
            var track = audioTracks[i].clone();
            mediastream.addTrack(track);
        }
        if (window.webkitURL) {
            source = window.webkitURL.createObjectURL(mediastream);
        } else {
            source = window.URL.createObjectURL(mediastream);
        }
        videoStreamDiv.src = source;
        var iconspan = document.createElement('span');
        iconspan.className = 'fa fa-microphone';
        button.appendChild(iconspan);
    }
}

function enableDisableAudioTrackFirefox(button, videoStreamDiv) {
    var audioTracks = mediastream.getAudioTracks();
    for (var i = 0, l = audioTracks.length; i < l; i++) {
        if (button.firstChild) {
            button.removeChild(button.firstChild);
        }
        if (!audioTracks[i].enabled) {
            var iconspan = document.createElement('span');
            iconspan.className = 'fa fa-microphone';
            button.appendChild(iconspan);
            console.log("Unmuting media stream ");
            audioTracks[i].enabled = true;
            audioTracks[i].muted = false;
        } else {
            var iconspan = document.createElement('span');
            iconspan.className = 'fa fa-microphone-slash';
            console.log("Muting media stream ");
            button.appendChild(iconspan);
            audioTracks[i].enabled = false;
            audioTracks[i].muted = true;
        }
    }
}
function addEventListenerBtn(button, videoStreamDiv) {
    button.addEventListener('click', function () {
        enableDisableAudioTrackFirefox(button, videoStreamDiv);
    });
}

function improveQuality(button, videoStreamDiv) {
    button.addEventListener('click', function () {
      //  enableDisableAudioTrackChrome(button, videoStreamDiv);
        qualityModify = true;
        videoImproved = videoStreamDiv;

        if (!!stream) {
            videoStreamDiv.src = null;
            stream.stop();
           // mainstream.stop();
         //   mediastream.stop();
        }
        buttonCalled = button;
        getUserMediaStream(640, 480, true);
    });
}

function addMuteEventListenerBtn(button, videoStreamDiv) {
    button.addEventListener('click', function () {
        enableDisableAudioTrackChrome(button, videoStreamDiv);
    });
}
