// connector class for connecting two or more peers.

// initializing the connector object for peer.
var remoteWaitingModalDisplayed = false;
var peer = new Peer({
    key: 'yu809arn510v0a4i',
    // host: '192.168.1.8', 
    // port: 8668,
    // path: '/peerjs',
    config: { 'iceServers': [
    { url: 'stun:stun01.sipphone.com' },
    { url: 'stun:stun.ekiga.net' },
    { url: 'stun:stun.fwdnet.net' },
    { url: 'stun:stun.ideasip.com' },
    { url: 'stun:stun.iptel.org' },
    { url: 'stun:stun.rixtelecom.se' },
    { url: 'stun:stun.schlund.de' },
    { url: 'stun:stun.l.google.com:19302' },
    { url: 'stun:stun1.l.google.com:19302' },
    { url: 'stun:stun2.l.google.com:19302' },
    { url: 'stun:stun3.l.google.com:19302' },
    { url: 'stun:stun4.l.google.com:19302' },
    { url: 'stun:stunserver.org' },
    { url: 'stun:stun.softjoys.com' },
    { url: 'stun:stun.voiparound.com' },
    { url: 'stun:stun.voipbuster.com' },
    { url: 'stun:stun.voipstunt.com' },
    { url: 'stun:stun.voxgratia.org' },
    { url: 'stun:stun.xten.com' },
    {
        url: 'turn:numb.viagenie.ca',
        credential: 'khandelwal',
        username: 'udit@yogaindoors.com'
    },
    {
        url: 'turn:192.158.29.39:3478?transport=udp',
        credential: 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
        username: '28224511:1379330808'
    },
    {
        url: 'turn:192.158.29.39:3478?transport=tcp',
        credential: 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
        username: '28224511:1379330808'
    }
  ]
    },

    debug: 3
});
var localPeerId;
var videoWidth = 1400;
var videoHeight = 900;
// all the remote peer ids along  with their videos are stored as a hashmap in this object
var callingPeers = new Object();
// opens up a peer socket and provides an Id for the same. This id is to be stored locally and relayed to the other
// peer, who would then join this peer.
peer.on('open', function (id) {
    localPeerId = id;
    if( typeof type == undefined || type !== "bclistener"){
       getUserMediaStream(videoWidth, videoHeight, false);   
    } else {
      sendRtcInfo(localPeerId, roombooked, usernametaken, type);   
    }
});


// registering call event on the peer. This allows the peer to receive call connections from the other peer and answer the call
peer.on('call', function (caller) {
    //  document.getElementById('answercall').innerHTML = "answering peer call";
    attendCall(caller);
});

//In case the peer gets errored out due to some reason. This is the part that handles it.
peer.on('error', function (err) {
    console.log("Error in connecting to the peer " + err);
});

//When the peer is disconnected, the same is reflected in the console. The peer can take no more connections now 
// and for new connections, it has to be instantiated again.
peer.on('disconnected', function () {
    console.log("The peer got disconnected and cannot accept any new connections.");
});


// It responds to the remote peer by providing its own stream.
function attendCall(caller) {

    // attend a peer's call by providing your main stream
    if(type === "bclistener"){
        caller.answer();
    }else {
        caller.answer(mainstream);
    }
    
    if (!remoteWaitingModalDisplayed) {
        $('#remoteWaitingModal').modal('show');
        remoteWaitingModalDisplayed = true;
    }
    caller.on('stream', function (stream) {
        var videoStreamDiv = setVideo(stream);
        console.log("Caller peer with id " + caller.peer);
        callingPeers[caller.peer] = videoStreamDiv;
        hideRemotePeerModalIfDisplayed();
    }, function (err) {
        console.log('Failed to get local stream', err);
    });

    caller.on('error', function (err) {
        if (!remoteWaitingModalDisplayed) {
            $('#remoteLoadingErrorModal').modal('show');
        } else {
            hideRemotePeerModalIfDisplayed();
            $('#remoteLoadingErrorModal').modal('show');
            remoteWaitingModalDisplayed = true;
        }
        console.log("Error in connecting caller to the peer!");
        var videoStreamDiv = callingPeers[caller.peer];
        videoStreamDiv.parentNode.removeChild(videoStreamDiv);
    });
    caller.on('close', function () {
        if (!remoteWaitingModalDisplayed) {
            $('#connectionClosedModal').modal('show');
        }
        console.log("Media stream track from the remote person is closed. hence removing the video tag");
        var videoStreamDiv = callingPeers[caller.peer];
        videoStreamDiv.parentNode.removeChild(videoStreamDiv);
    });
}

// It sends a connection request to the remote peer to join the conference.
function joinPeer(remotePeerId, type) {

    // sending local stream to the remote peer to join.
    var call = peer.call(remotePeerId, mainstream);

    if (call != null) {
        if (type != "bc" && !remoteWaitingModalDisplayed) {
            $('#remoteWaitingModal').modal('show');
            remoteWaitingModalDisplayed = true;
        }
        call.on('stream', function (stream) {
            if(stream != null){
                console.log("Call peer with id " + call.peer);
            var videoStreamDiv = setVideo(stream);
            callingPeers[call.peer] = videoStreamDiv;
            }
            hideRemotePeerModalIfDisplayed();
        }, function (err) {
            console.log('Failed to get local stream', err);
        });
        call.on('error', function (err) {
            if (!remoteWaitingModalDisplayed) {
                $('#reconnectionModal').modal('show');
            } else {
                hideRemotePeerModalIfDisplayed();
                $('#reconnectionModal').modal('show');
            }
            console.log("Error in connecting call to the peer!");
            var videoStreamDiv = callingPeers[call.peer];
            videoStreamDiv.parentNode.removeChild(videoStreamDiv);
        });

        call.on('close', function () {
            if (!remoteWaitingModalDisplayed) {
                $('#connectionClosedModal').modal('show');
            }
            var videoStreamDiv = callingPeers[call.peer];
            videoStreamDiv.parentNode.removeChild(videoStreamDiv);
        });
    } else {
        $("#reconnectionModal").modal('show');
    }
}

// method to reconnect to the disconnected peer. This might be used later. currently does not serve much purpose.
function reconnect() {
    $('#reconnectionModal').modal('hide');
}


function hideRemotePeerModalIfDisplayed() {
    if (remoteWaitingModalDisplayed) {
        if ($("#remoteLoadingErrorModal") != null && $("#remoteLoadingErrorModal").data('bs.modal') != null
                && $("#remoteLoadingErrorModal").data('bs.modal').isShown) {
            $('#remoteLoadingErrorModal').modal('hide');
            $('#remoteLoadingErrorModal').on('hidden.bs.modal', '.modal', function () {
                $(this).removeData('bs.modal');
            });
        }
        if ($("#remoteWaitingModal") != null && $("#remoteWaitingModal").data('bs.modal') != null &&
                $("#remoteWaitingModal").data('bs.modal').isShown) {
            $('#remoteWaitingModal').modal('hide');
            $('#remoteWaitingModal').on('hidden.bs.modal', '.modal', function () {
                $(this).removeData('bs.modal');
            });
        }
        remoteWaitingModalDisplayed = false;
    }
}
    