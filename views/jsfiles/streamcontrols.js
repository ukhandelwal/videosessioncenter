var mainstream;
navigator.getUserMedia || (navigator.getUserMedia = navigator.mozGetUserMedia
                                 || navigator.webkitGetUserMedia || navigator.msGetUserMedia);
function getUserMediaStream(videoWidth, videoHeight, improve) {
    // This check makes sure that getUserMedia is available.
    if (navigator.getUserMedia) {
        // this is to test if the person is opening the site in a mobile browser.
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            isPc = false;
            var qvgaConstraints = {
                video: {
                    mandatory: {
                        maxWidth: videoWidth,
                        maxHeight: videoHeight,
                        minWidth: 160,
                        minHeight: 90,
                        maxFrameRate: 60,
                        minFrameRate: 10
                    }
                },
                audio: true

            };
            // access the media of the phone.
            navigator.getUserMedia(qvgaConstraints, onSuccess, onError);
        } else {
            isPc = true;
            var qvgaConstraints = {
                video: {
                    mandatory: {
                        maxWidth: videoWidth,
                        maxHeight: videoHeight,
                        minWidth: 160,
                        minHeight: 90,
                        maxFrameRate: 60,
                        minFrameRate: 10
                    }
                },
                audio: true

            };
            //access the media of the computer.
            navigator.getUserMedia(qvgaConstraints, onSuccess, onError);
        }
    } else {
        alert("getUserMedia not supported");
    }
}

function onSuccess(stream) {

    var browser = navigator.userAgent.toLowerCase();
    //some of the operations are firefox centric and hence the check.
    if (browser.indexOf('firefox') > -1) {
        isFirefox = true;
    } else {
        isFirefox = false;
    }


    if (typeof stream.clone === 'function') {
        console.log("The track is cloned and hence is safe to mute!");
        mediastream = stream.clone();
    } else {
        // to be enabled for remote testing purposes only.
        console.log("Cloning is not supported in this browser!!");
        mediastream = stream;
    }
    mainstream = stream;
    // i have joined. send this information to the server.
    setVideoLocal(mediastream);
    sendRtcInfo(localPeerId, roombooked, usernametaken, type);
}
function onError(stream) {
    alert("error in " + stream);
}

function hideModal() {
    $('#waitingModal').modal('hide');
    $('#waitingModal').on('hidden.bs.modal', '.modal', function () {
        $(this).removeData('bs.modal');
    });
}

function setVideoLocal(stream) {
    var source;
    var videoStream;
    var button;
    videoStream = document.createElement("video");
    var division = document.createElement("div");
    division.appendChild(videoStream);
    if (isFirefox) {
        button = getMicMuteButton();
        division.appendChild(button);
        addEventListenerBtn(button, videoStream);
        enableDisableAudioTrackFirefox(button, videoStream);
    } else {
        disableAudioTrackChrome(videoStream);
    }
    document.getElementById('main-playing-video').appendChild(division);
    if (window.webkitURL) {
        source = window.webkitURL.createObjectURL(stream);
    } else {
        source = window.URL.createObjectURL(stream);
    }
    videoStream.src = source;
    videoStream.setAttribute("controls", "controls");
    videoStream.autoplay = true;
}


function setVideo(stream) {
    var source;
    var videoStream;
    var button;
    if (videoStream == null) {
        videoStream = document.createElement("video");
        var division = document.createElement("div");
        division.classList.add("video-container");
        division.appendChild(videoStream);
        document.getElementById('main-playing-video').appendChild(division);
    }

    if (window.webkitURL) {
        source = window.webkitURL.createObjectURL(stream);
    } else {
        source = window.URL.createObjectURL(stream);
    }
    videoStream.src = source;
    videoStream.setAttribute("controls", "controls");
    videoStream.autoplay = true;
    return videoStream;
}

function getMicMuteButton() {
    var button = document.createElement('button');
    button.setAttribute("class", "mutebutton btn btn-default btn-primary");
    var iconspan = document.createElement('span');
    iconspan.className = 'fa fa-microphone';
    button.appendChild(iconspan);
    return button;
}

function disableAudioTrackChrome(videoStreamDiv) {
    if (mediastream.getAudioTracks().length > 0) {
        // time to mute the track as it was unmuted before.
        var audioTracks = mediastream.getAudioTracks();
        for (var i = 0; i < audioTracks.length; i++) {
            console.log("Muting media stream ");
            audioTracks[i].stop();
            mediastream.removeTrack(audioTracks[i]);
        }
    }
}

function enableDisableAudioTrackFirefox(button, videoStreamDiv) {
    var audioTracks = mediastream.getAudioTracks();
    for (var i = 0, l = audioTracks.length; i < l; i++) {
        if (button.firstChild) {
            button.removeChild(button.firstChild);
        }
        if (!audioTracks[i].enabled) {
            var iconspan = document.createElement('span');
            iconspan.className = 'fa fa-microphone';
            button.appendChild(iconspan);
            console.log("Unmuting media stream ");
            audioTracks[i].enabled = true;
            audioTracks[i].muted = false;
        } else {
            var iconspan = document.createElement('span');
            iconspan.className = 'fa fa-microphone-slash';
            console.log("Muting media stream ");
            button.appendChild(iconspan);
            audioTracks[i].enabled = false;
            audioTracks[i].muted = true;
        }
    }
}
function addEventListenerBtn(button, videoStreamDiv) {
    button.addEventListener('click', function () {
        enableDisableAudioTrackFirefox(button, videoStreamDiv);
    });
}

