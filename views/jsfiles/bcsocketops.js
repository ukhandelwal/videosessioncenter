var socket = io.connect('http://vscultimate.azurewebsites.net', {'forceNew':true });
//var socket = io.connect('192.168.1.7', {'forceNew':true });
//var socket = io.connect('http://localhost/', {'forceNew':true });

// to be used to get out of the room.
function leaveroom() {
    socket.disconnect();
}

// whenever there is a new remote person joining in, this event is triggered in the server.
// the server then broadcasts information to only the broadcaster to share its peerid.
socket.on('bcjoinee', function (data) {
    console.log(data);
    var userObject = JSON.parse(data);
    newJoineeChatInsert(userObject);
    joinPeer(userObject.videoid, userObject.type);
});


// when a peer joins a room and opens up his media connection, this information is passed to the server using the userconnect event.
// the server would then broadcast it through the above method.
function sendRtcInfo(id, room, user, videotype) {
    var packet = {
        roomname: room,
        username: user,
        videoid: id,
        type: videotype
    }
    socket.emit('userconnect', JSON.stringify(packet));
    console.log('Information sent to server: id ' + id + " and the roomname " + room);
}