if ('content' in document.createElement('template')) {
    console.log("templates are supported");
} else {
    console.log("templates are not supported");
}

function insertLeftContent(data) {
    var userObject = JSON.parse(data);
    var tmpl = document.getElementById('left-template').content.cloneNode(true);

    if(tmpl.querySelector('.username') != null){
        tmpl.querySelector('.username').innerText = userObject.username;
        tmpl.querySelector('.username').textContent = userObject.username;
    } else {
        tmpl.content.querySelector('.username').textContent = userObject.username;
    }    
    var date = new Date(userObject.chattime);
    var time = getDateTimeInFormat(date);

    if(tmpl.querySelector('.chattime') != null){
         tmpl.querySelector('.chattime').innerText = time;
         tmpl.querySelector('.chattime').textContent = time;

    } else {
        tmpl.content.querySelector('.chattime').textContent = time;
    }
    if(tmpl.querySelector('.chatmessage') != null){
         tmpl.querySelector('.chatmessage').innerText = userObject.chatmessage;
         tmpl.querySelector('.chatmessage').textContent = userObject.chatmessage;
    } else {
        tmpl.content.querySelector('.chatmessage').textContent = userObject.chatmessage;
    }
    if(tmpl.querySelector('.userimage') != null){
          tmpl.querySelector('.userimage').src = "http://placehold.it/50/55C1E7/fff&text="+ userObject.username.charAt(0);
    } else {
        tmpl.content.querySelector('.userimage').src = "http://placehold.it/50/55C1E7/fff&text="+ userObject.username.charAt(0);
    }
    var chatBody = document.getElementById('chatbody');
    
    chatBody.appendChild(tmpl);
}

function getDateTimeInFormat(date) {
    var time = date.getDate() + "-" + (date.getMonth() + 1) + "-" + (date.getYear()+1900) + " " + date.getHours() +
     ":" + date.getMinutes();
    return time;
}
function newJoineeChatInsert(userObject) {
    //new JSONObject();
    var tmpl = document.getElementById('newjoineeChat').content.cloneNode(true);
    tmpl.querySelector('.newuserintro').innerText = userObject.username;
    var chatBody = document.getElementById('chatbody');
    chatBody.appendChild(tmpl);
}
function insertRightContent(data) {
    var userObject = JSON.parse(data);
    var tmpl = document.getElementById('right-template').content.cloneNode(true);

    if(tmpl.querySelector('.username') != null){
        tmpl.querySelector('.username').innerText = userObject.username;
    } else {
        tmpl.content.querySelector('.username').innerText = userObject.username;
    }    
    var date = new Date(userObject.chattime);
    var time = getDateTimeInFormat(date);

    if(tmpl.querySelector('.chattime') != null){
         tmpl.querySelector('.chattime').innerText = time;
    } else {
        tmpl.content.querySelector('.chattime').innerText = time;
    }
    if(tmpl.querySelector('.chatmessage') != null){
         tmpl.querySelector('.chatmessage').innerText = userObject.chatmessage;
    } else {
        tmpl.content.querySelector('.chatmessage').innerText = userObject.chatmessage;
    }
    if(tmpl.querySelector('.userimage') != null){
          tmpl.querySelector('.userimage').src = "http://placehold.it/50/55C1E7/fff&text="+ userObject.username.charAt(0);
    } else {
        tmpl.content.querySelector('.userimage').src = "http://placehold.it/50/55C1E7/fff&text="+ userObject.username.charAt(0);
    }
    var chatBody = document.getElementById('chatbody');
    chatBody.appendChild(tmpl);
}

function sendChatToClients() {

    var data = document.getElementById('btn-input').value;
    console.log(data + " in the room " + roombooked + " username " + usernametaken);
    var object = {
        roomname: roombooked,
        username: usernametaken,
        chatmessage: data,
        chattime: new Date()
    }
    sendChat(JSON.stringify(object));
    document.getElementById('btn-input').value = "";
}
function sendChat(data) {
    insertRightContent(data);
    socket.emit('sendChat', data);
}
socket.on('receiveChat', function (data) {
    insertLeftContent(data);
});   


function submitText(event){
    // enter pressed.
    if(event.keyCode == 13){
        sendChatToClients();
    }
}
 