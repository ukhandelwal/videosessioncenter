var express = require('express');
var app = express();
var io = require('socket.io');
var http = require('http');
var videoroom = require("./models/videoroom");
var chatmodel = require("./models/chatmodel");
var path = require('path');

// socket.io 
var server = require('http').Server(app);
var io = require('socket.io')(server);

// ejs inclusion.
var ejs = require('ejs');
app.engine('html', require('ejs').renderFile);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

//server.listen(process.env.PORT || 8080);
server.listen(8124, "192.168.1.7");
// json parsing code.
var parser = require("body-parser");
app.use(parser.urlencoded({ extended: true }));
app.use(parser.json());

app.use(express.static(path.join(__dirname, 'public')));

// enabling access control origin
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/', function (req, res) {
    res.render('index');
});

app.get('/instructor', function (req, res) {
    res.render('instructor');
});

app.get('/pupil', function (req, res) {
    res.render('pupil');
});

app.get('/mediastreamops.js', function (req, res) {
    res.sendFile('mediastreamops.js', { root: __dirname + '/views/jsfiles' });
});

app.get('/streamcontrols.js', function (req, res) {
    res.sendFile('streamcontrols.js', { root: __dirname + '/views/jsfiles' });
});

app.get('/peerconnector.js', function (req, res) {
    res.sendFile('peerconnector.js', { root: __dirname + '/views/jsfiles' });
});

app.get('/peer.js', function (req, res) {
    res.sendFile('peer.js', { root: __dirname + '/views/jsfiles' });
});

app.get('/socketops.js', function (req, res) {
    res.sendFile('socketops.js', { root: __dirname + '/views/jsfiles' });
});

app.get('/bcsocketops.js', function (req, res) {
    res.sendFile('bcsocketops.js', { root: __dirname + '/views/jsfiles' });
});

app.get('/templating.js', function (req, res) {
    res.sendFile('templating.js', { root: __dirname + '/views/jsfiles' });
});

app.get('/chatpanel.js', function (req, res) {
    res.sendFile('chatpanel.js', { root: __dirname + '/views/jsfiles' });
});

io.on('connection', function (socket) {
    console.log("user is connected");
    socket.on('userconnect', function (data) {

        var videodata = new videoroom(JSON.parse(data));
        console.log(videodata.videoid);
        socket.join(videodata.roomname);
        var videotype = videodata.type;
        if (videotype === "bclistener" || videotype === "ic") {
            // send to all clients except the one who joined the conversation.
            socket.broadcast.to(videodata.roomname).emit('bcjoinee', JSON.stringify(videodata));
            // io.in(videodata.roomname).emit('newjoinee', videodata.id);
        } else if (videotype === "vc") {
            // send to all clients except the one who joined the conversation.
            socket.broadcast.to(videodata.roomname).emit('newjoinee', JSON.stringify(videodata));
            // io.in(videodata.roomname).emit('newjoinee', videodata.id);
        } 
    });
    socket.on('sendChat', function (data) {
        var object = new chatmodel(JSON.parse(data));
        console.log("Sending chat to everybody in the room " + object.roomname);
        socket.broadcast.to(object.roomname).emit('receiveChat', data);
    });
    socket.on('disconnect', function () {
        console.log("user has been disconnected");
    });

});

app.post('/session', function (req, res) {
    // res.send(" data " + req.query.username + "  " +req.query.roomname);
    var variables = {
        roombooked: req.body.session.roomname,
        usernametaken: req.body.session.username
    };
    if (req.body.vcsubmit) {
        console.log(req.body.vcsubmit);
        res.render('conferencing', variables);
    } else if (req.body.bcsubmit) {
        console.log(req.body.bcsubmit);
        res.render('broadcaster', variables);
    } else {
        console.log(req.body.icsubmit);
        res.render('broadcaster', variables);
    }
})


app.post('/joining', function (req, res) {
    // res.send(" data " + req.query.username + "  " +req.query.roomname);
    var variables = {
        roombooked: req.body.pupil.roomname,
        usernametaken: req.body.pupil.username
    };
    if (req.body.vcsubmit) {
        console.log(req.body.vcsubmit);
        res.render('conferencing', variables);
    } else if (req.body.bcsubmit) {
        console.log(req.body.bcsubmit);
        res.render('viewbroadcast', variables);
    } else {
        console.log(req.body.icsubmit);
        res.render('instructorled', variables);
    }
})