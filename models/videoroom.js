function videoroom(data){
    this.roomname = data.roomname;
    this.videoid = data.videoid;
    this.username = data.username;
    this.type = String(data.type);
    console.log(this.roomname + " is the room with id " + this.username + " type " + this.type);
}
module.exports =videoroom;