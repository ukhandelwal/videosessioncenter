function videoroom(data){
    this.roomname = data.roomname;
    this.chattime = data.chattime;
    this.username = data.username;
    this.chatmessage = data.chatmessage;
    console.log(this.roomname + " is the room with id " + this.username + " and the chat message " + this.chatmessage);
}
module.exports =videoroom;